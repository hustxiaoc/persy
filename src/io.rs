use crate::PRes;
use byteorder::{BigEndian, ByteOrder, ReadBytesExt, WriteBytesExt};
use integer_encoding::VarInt;
use std::io::{Cursor, Read, Write};

pub trait InfallibleRead {
    fn read_exact(&mut self, buf: &mut [u8]);
}

pub trait InfallibleWrite {
    fn write_all(&mut self, buf: &[u8]);
}

#[allow(dead_code)]
pub(crate) fn write_varint<V: VarInt, W: InfallibleWrite + ?Sized>(w: &mut W, v: V) {
    let mut buf = [0 as u8; 10];
    let b = v.encode_var(&mut buf);
    w.write_all(&buf[0..b])
}

// Start of snippet taken from integer-ecoding specialized for InfallibleRead
pub const MSB: u8 = 0b1000_0000;

#[allow(dead_code)]
pub(crate) fn read_varint<V: VarInt, R: InfallibleRead + ?Sized>(r: &mut R) -> V {
    let mut int_buf = [0 as u8; 10];
    let mut int_cursor = 0;
    let mut buf = [0 as u8; 1];

    while int_cursor == 0 || (int_buf[int_cursor - 1] & MSB != 0) {
        r.read_exact(&mut buf);
        if int_cursor >= 10 {
            panic!("Unterminated varint")
        }
        int_buf[int_cursor] = buf[0];
        int_cursor = 0;
    }

    V::decode_var(&int_buf[0..int_cursor]).0
}

// End of snippet taken from integer-ecoding specialized for InfallibleRead

pub(crate) trait InfallibleReadVarInt: InfallibleRead {
    #[inline]
    fn read_u8_varint(&mut self) -> u8 {
        read_varint(self)
    }

    #[inline]
    fn read_u16_varint(&mut self) -> u16 {
        read_varint(self)
    }

    #[inline]
    fn read_u32_varint(&mut self) -> u32 {
        read_varint(self)
    }

    #[inline]
    fn read_u64_varint(&mut self) -> u64 {
        read_varint(self)
    }

    #[inline]
    fn read_i8_varint(&mut self) -> i8 {
        read_varint(self)
    }

    #[inline]
    fn read_i16_varint(&mut self) -> i16 {
        read_varint(self)
    }

    #[inline]
    fn read_i32_varint(&mut self) -> i32 {
        read_varint(self)
    }

    #[inline]
    fn read_i64_varint(&mut self) -> i64 {
        read_varint(self)
    }
}
impl<R: InfallibleRead + ?Sized> InfallibleReadVarInt for R {}

pub(crate) trait InfallibleWriteVarInt: InfallibleWrite {
    #[inline]
    fn write_varint_u8(&mut self, value: u8) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_u16(&mut self, value: u16) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_u32(&mut self, value: u32) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_u64(&mut self, value: u64) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_i8(&mut self, value: i8) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_i16(&mut self, value: i16) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_i32(&mut self, value: i32) {
        write_varint(self, value)
    }

    #[inline]
    fn write_varint_i64(&mut self, value: i64) {
        write_varint(self, value)
    }
}

impl<W: InfallibleWrite + ?Sized> InfallibleWriteVarInt for W {}

pub(crate) trait InfallibleWriteFormat: InfallibleWrite {
    #[inline]
    fn write_u8(&mut self, value: u8) {
        self.write_all(&[value])
    }

    #[inline]
    fn write_u16(&mut self, value: u16) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_u32(&mut self, value: u32) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_u64(&mut self, value: u64) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_u128(&mut self, value: u128) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i8(&mut self, value: i8) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i16(&mut self, value: i16) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i32(&mut self, value: i32) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i64(&mut self, value: i64) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_i128(&mut self, value: i128) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_f32(&mut self, value: f32) {
        self.write_all(&value.to_be_bytes())
    }

    #[inline]
    fn write_f64(&mut self, value: f64) {
        self.write_all(&value.to_be_bytes())
    }
}

impl<W: InfallibleWrite + ?Sized> InfallibleWriteFormat for W {}

impl InfallibleWrite for Vec<u8> {
    fn write_all(&mut self, buf: &[u8]) {
        self.extend_from_slice(buf);
    }
}

pub(crate) trait InfallibleReadFormat: InfallibleRead {
    #[inline]
    fn read_u8(&mut self) -> u8 {
        let mut value = [0; 1];
        self.read_exact(&mut value);
        value[0]
    }

    #[inline]
    fn read_u16(&mut self) -> u16 {
        let mut value = [0; 2];
        self.read_exact(&mut value);
        u16::from_be_bytes(value)
    }

    #[inline]
    fn read_u32(&mut self) -> u32 {
        let mut value = [0; 4];
        self.read_exact(&mut value);
        u32::from_be_bytes(value)
    }

    #[inline]
    fn read_u64(&mut self) -> u64 {
        let mut value = [0; 8];
        self.read_exact(&mut value);
        u64::from_be_bytes(value)
    }

    #[inline]
    fn read_u128(&mut self) -> u128 {
        let mut value = [0; 16];
        self.read_exact(&mut value);
        u128::from_be_bytes(value)
    }

    #[inline]
    fn read_i8(&mut self) -> i8 {
        let mut value = [0; 1];
        self.read_exact(&mut value);
        i8::from_be_bytes(value)
    }

    #[inline]
    fn read_i16(&mut self) -> i16 {
        let mut value = [0; 2];
        self.read_exact(&mut value);
        i16::from_be_bytes(value)
    }

    #[inline]
    fn read_i32(&mut self) -> i32 {
        let mut value = [0; 4];
        self.read_exact(&mut value);
        i32::from_be_bytes(value)
    }

    #[inline]
    fn read_i64(&mut self) -> i64 {
        let mut value = [0; 8];
        self.read_exact(&mut value);
        i64::from_be_bytes(value)
    }

    #[inline]
    fn read_i128(&mut self) -> i128 {
        let mut value = [0; 16];
        self.read_exact(&mut value);
        i128::from_be_bytes(value)
    }

    #[inline]
    fn read_f32(&mut self) -> f32 {
        let mut value = [0; 4];
        self.read_exact(&mut value);
        f32::from_be_bytes(value)
    }

    #[inline]
    fn read_f64(&mut self) -> f64 {
        let mut value = [0; 8];
        self.read_exact(&mut value);
        f64::from_be_bytes(value)
    }
}

impl<R: InfallibleRead + ?Sized> InfallibleReadFormat for R {}

impl<T> InfallibleRead for Cursor<T>
where
    T: AsRef<[u8]>,
{
    fn read_exact(&mut self, buf: &mut [u8]) {
        Read::read_exact(self, buf).expect("never fail in a in memory buffer");
    }
}

pub(crate) fn read_u16(buf: &[u8]) -> u16 {
    BigEndian::read_u16(buf)
}
pub(crate) fn write_u16(buf: &mut [u8], val: u16) {
    BigEndian::write_u16(buf, val)
}
pub(crate) fn read_u64(buf: &[u8]) -> u64 {
    BigEndian::read_u64(buf)
}
pub(crate) fn write_u64(buf: &mut [u8], val: u64) {
    BigEndian::write_u64(buf, val)
}

pub(crate) trait WriteFormat: Write {
    #[inline]
    fn write_u8(&mut self, value: u8) -> PRes<()> {
        Ok(WriteBytesExt::write_u8(self, value)?)
    }

    #[inline]
    fn write_u16(&mut self, value: u16) -> PRes<()> {
        Ok(WriteBytesExt::write_u16::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_u32(&mut self, value: u32) -> PRes<()> {
        Ok(WriteBytesExt::write_u32::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_u64(&mut self, value: u64) -> PRes<()> {
        Ok(WriteBytesExt::write_u64::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_u128(&mut self, value: u128) -> PRes<()> {
        Ok(WriteBytesExt::write_u128::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i8(&mut self, value: i8) -> PRes<()> {
        Ok(WriteBytesExt::write_i8(self, value)?)
    }

    #[inline]
    fn write_i16(&mut self, value: i16) -> PRes<()> {
        Ok(WriteBytesExt::write_i16::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i32(&mut self, value: i32) -> PRes<()> {
        Ok(WriteBytesExt::write_i32::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i64(&mut self, value: i64) -> PRes<()> {
        Ok(WriteBytesExt::write_i64::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_i128(&mut self, value: i128) -> PRes<()> {
        Ok(WriteBytesExt::write_i128::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_f32(&mut self, value: f32) -> PRes<()> {
        Ok(WriteBytesExt::write_f32::<BigEndian>(self, value)?)
    }

    #[inline]
    fn write_f64(&mut self, value: f64) -> PRes<()> {
        Ok(WriteBytesExt::write_f64::<BigEndian>(self, value)?)
    }
}

impl<W: Write + ?Sized> WriteFormat for W {}

pub(crate) trait ReadFormat: Read {
    #[inline]
    fn read_u8(&mut self) -> PRes<u8> {
        Ok(ReadBytesExt::read_u8(self)?)
    }

    #[inline]
    fn read_u16(&mut self) -> PRes<u16> {
        Ok(ReadBytesExt::read_u16::<BigEndian>(self)?)
    }

    #[inline]
    fn read_u32(&mut self) -> PRes<u32> {
        Ok(ReadBytesExt::read_u32::<BigEndian>(self)?)
    }

    #[inline]
    fn read_u64(&mut self) -> PRes<u64> {
        Ok(ReadBytesExt::read_u64::<BigEndian>(self)?)
    }

    #[inline]
    fn read_u128(&mut self) -> PRes<u128> {
        Ok(ReadBytesExt::read_u128::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i8(&mut self) -> PRes<i8> {
        Ok(ReadBytesExt::read_i8(self)?)
    }

    #[inline]
    fn read_i16(&mut self) -> PRes<i16> {
        Ok(ReadBytesExt::read_i16::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i32(&mut self) -> PRes<i32> {
        Ok(ReadBytesExt::read_i32::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i64(&mut self) -> PRes<i64> {
        Ok(ReadBytesExt::read_i64::<BigEndian>(self)?)
    }

    #[inline]
    fn read_i128(&mut self) -> PRes<i128> {
        Ok(ReadBytesExt::read_i128::<BigEndian>(self)?)
    }

    #[inline]
    fn read_f32(&mut self) -> PRes<f32> {
        Ok(ReadBytesExt::read_f32::<BigEndian>(self)?)
    }

    #[inline]
    fn read_f64(&mut self) -> PRes<f64> {
        Ok(ReadBytesExt::read_f64::<BigEndian>(self)?)
    }
}

impl<R: Read + ?Sized> ReadFormat for R {}
