use crate::{
    error::PRes,
    index::{
        config::{IndexType, ValueMode},
        keeper::{IndexKeeper, IndexModify},
        nodes::{Leaf, Node, NodeRef, Nodes, PageIter, PageIterBack, Value},
    },
};
use std::{cmp::Ordering, ops::Bound, rc::Rc};

pub enum ValueChange<V> {
    ADD(V),
    REMOVE(Option<V>),
}

pub struct KeyChanges<K, V> {
    pub k: K,
    changes: Vec<ValueChange<V>>,
}

impl<K: IndexType, V: IndexType> KeyChanges<K, V> {
    #[allow(dead_code)]
    pub fn single_add(k: K, v: V) -> KeyChanges<K, V> {
        KeyChanges {
            k,
            changes: vec![ValueChange::ADD(v)],
        }
    }

    #[allow(dead_code)]
    fn single_delete(k: K, v: Option<V>) -> KeyChanges<K, V> {
        KeyChanges {
            k,
            changes: vec![ValueChange::REMOVE(v)],
        }
    }

    pub fn new(k: K, changes: Vec<ValueChange<V>>) -> KeyChanges<K, V> {
        KeyChanges { k, changes }
    }
    fn apply(&self, leaf: &mut Leaf<K, V>, value_mode: ValueMode, index: &str) -> PRes<bool> {
        let mut update = false;
        for vc in &self.changes {
            update |= match vc {
                ValueChange::ADD(v) => {
                    if leaf.len() > 0 {
                        leaf.insert_or_update(&self.k, &v, value_mode.clone(), index)?;
                    } else {
                        leaf.add(0, &self.k, v, value_mode.clone());
                    }
                    true
                }
                ValueChange::REMOVE(r) => leaf.remove(&self.k, &r),
            };
        }
        Ok(update)
    }
}

pub trait Index<K: IndexType, V: IndexType> {
    fn get(&mut self, k: &K) -> PRes<Option<Value<V>>>;
    fn iter_from(&mut self, first: Bound<&K>) -> PRes<PageIter<K, V>>;
    fn back_iter_from(&mut self, first: Bound<&K>) -> PRes<PageIterBack<K, V>>;
}

pub trait IndexApply<K: IndexType, V: IndexType>: Index<K, V> {
    fn apply(&mut self, adds: &[KeyChanges<K, V>]) -> PRes<()>;
}

#[derive(PartialEq, Clone, Debug)]
pub struct PosRef<K: IndexType> {
    pub k: K,
    pub pos: usize,
    pub node_ref: NodeRef,
    pub sibling: Option<NodeRef>,
    pub version: u16,
}

impl<K: IndexType> PosRef<K> {
    pub(crate) fn new(k: &K, pos: usize, node_ref: NodeRef, sibling: Option<NodeRef>) -> PosRef<K> {
        PosRef {
            k: k.clone(),
            pos,
            node_ref,
            sibling,
            version: 0,
        }
    }
}

struct ParentNodeChanged<K: IndexType> {
    path: Vec<PosRef<K>>,
    children: Vec<PosRef<K>>,
}

fn split_link_save<K: IndexType, V: IndexType, I: IndexModify<K, V>>(
    index: &mut I,
    mut prev_id: NodeRef,
    mut first: Node<K, V>,
    cur_version: u16,
) -> PRes<Vec<(K, NodeRef)>> {
    let new_nodes = first.split(index.top_limit());
    let mut ids = Vec::new();
    for (k, new_node) in new_nodes {
        let saved_id = index.insert(new_node)?;
        ids.push((k, saved_id));
    }
    if let Node::LEAF(leaf) = first {
        let mut prev_leaf = leaf;
        let mut prev_version = cur_version;
        for (_, id) in &ids {
            // Just unwrap, the leaf should be locked so expected to exist
            let (sibling_node, version) = index.load_modify(&id)?.unwrap();
            if let Node::LEAF(sibling) = index.owned(&id, sibling_node) {
                index.update(&prev_id, Node::LEAF(prev_leaf), prev_version)?;
                prev_id = id.clone();
                prev_leaf = sibling;
                prev_version = version;
            }
        }
        index.update(&prev_id, Node::LEAF(prev_leaf), prev_version)?;
    } else {
        index.update(&prev_id, first, cur_version)?;
    }
    Ok(ids)
}

fn group_by_parent<K: IndexType>(updates: Vec<Vec<PosRef<K>>>) -> Vec<ParentNodeChanged<K>> {
    let mut parent_updates = Vec::new();
    let mut parent_node: Option<NodeRef> = None;
    let mut new_update: Option<ParentNodeChanged<K>> = None;
    for mut update in updates {
        if let Some(last) = update.pop() {
            if parent_node == update.last().map(|x| x.node_ref.clone()) {
                if let Some(p) = &mut new_update {
                    p.children.push(last);
                }
            } else {
                if let Some(p) = new_update {
                    parent_updates.push(p);
                }
                parent_node = update.last().map(|x| x.node_ref.clone());
                let mut children = Vec::new();
                children.push(last);
                new_update = Some(ParentNodeChanged { path: update, children });
            }
        }
    }
    if let Some(p) = new_update {
        parent_updates.push(p);
    }
    parent_updates
}
fn lock_parents<K: IndexType, V: IndexType, I: IndexModify<K, V>>(
    index: &mut I,
    mut updates: Vec<Vec<PosRef<K>>>,
) -> PRes<Vec<Vec<PosRef<K>>>> {
    for update in &mut updates {
        loop {
            debug_assert!(
                update.len() >= 2,
                "never lock a path that shorter than 2 because that would be the root",
            );
            let locked = {
                let update = &update;
                let len = update.len();
                let rec_ref = &update[len - 2];
                if let Some((node, version)) = index.load_modify(&rec_ref.node_ref)? {
                    lock_logic(index, rec_ref, node, version)?.is_some()
                } else {
                    false
                }
            };
            if locked {
                break;
            } else {
                let last = update.last().unwrap().clone();
                if let Some(ref node) = index.get_root_refresh()? {
                    let mut path = Vec::new();
                    let mut cur_node = PosRef::new(&last.k, 0, node.clone(), None);
                    while cur_node.node_ref != last.node_ref {
                        let mut restart = true;
                        if let Some((node_modify, version)) = index.load_modify(&cur_node.node_ref)? {
                            if let Node::NODE(n) = &*node_modify {
                                cur_node.version = version;
                                path.push(cur_node.clone());
                                if let Some(x) = n.find_write(&last.k) {
                                    cur_node = x;
                                    restart = false;
                                }
                            }
                        }
                        if restart {
                            if let Some(ref node) = index.get_root_refresh()? {
                                cur_node = PosRef::new(&last.k, 0, node.clone(), None);
                                path.clear();
                            } else {
                                panic!("restart node finding but not root present");
                            }
                        }
                    }
                    path.push(last);
                    *update = path;
                }
            }
        }
    }
    Ok(updates)
}

fn merge_and_save<K: IndexType, V: IndexType, I: IndexModify<K, V>>(
    index: &mut I,
    parent: &mut Nodes<K>,
    pos: usize,
    mut cur: Node<K, V>,
    version: u16,
) -> PRes<bool> {
    Ok(if pos > 0 {
        let node_ref = parent.get(pos - 1);
        let (dest_node, dest_version) = index.load_modify(&node_ref)?.unwrap();
        let mut dest_merge = index.owned(&node_ref, dest_node);
        dest_merge.merge_right(parent.get_key(pos - 1), &mut cur);
        index.update(&node_ref, dest_merge, dest_version)?;
        index.delete(&parent.remove(pos).unwrap(), version)?;
        false
    } else {
        let node_ref = parent.get(pos + 1);
        let (source_node, source_version) = index.load_modify(&node_ref)?.unwrap();
        let mut source_merge = index.owned(&node_ref, source_node);
        cur.merge_right(parent.get_key(pos), &mut source_merge);
        index.delete(&parent.remove(pos + 1).unwrap(), source_version)?;
        index.update(&parent.get(pos), cur, version)?;
        true
    })
}

fn lock_logic<K: IndexType, V: IndexType, I: IndexModify<K, V>>(
    index: &mut I,
    cur_node: &PosRef<K>,
    node: Rc<Node<K, V>>,
    node_version: u16,
) -> PRes<Option<(Rc<Node<K, V>>, u16)>> {
    if let Some(sibling) = &cur_node.sibling {
        let sibling_load = if let Some(load) = index.load_modify(sibling)? {
            load
        } else {
            return Ok(None);
        };
        let ((first, second), (f, s)) = if cur_node.pos > 0 {
            ((sibling, &cur_node.node_ref), (sibling_load, (node, node_version)))
        } else {
            ((&cur_node.node_ref, sibling), ((node, node_version), sibling_load))
        };

        let (first_node, first_version) = f;
        let (second_node, second_version) = s;

        if let (Some(f), Some(s)) = (first_node.get_next(), second_node.get_prev()) {
            if f.cmp(s) != Ordering::Equal {
                return Ok(None);
            }
        } else {
            return Ok(None);
        }
        if cur_node.pos == 0 && !first_node.check_range(&cur_node.k) {
            return Ok(None);
        }
        if cur_node.pos > 0 && !second_node.check_range(&cur_node.k) {
            return Ok(None);
        }
        if !index.lock(first, first_version)? {
            return Ok(None);
        }
        if !index.lock(second, second_version)? {
            index.unlock(first)?;
            return Ok(None);
        }
        if cur_node.pos > 0 {
            Ok(Some((second_node, second_version)))
        } else {
            Ok(Some((first_node, first_version)))
        }
    } else if !node.check_range(&cur_node.k) {
        Ok(None)
    } else if index.lock(&cur_node.node_ref, node_version)? {
        Ok(Some((node, node_version)))
    } else {
        Ok(None)
    }
}

impl<K: IndexType, V: IndexType, T> IndexApply<K, V> for T
where
    T: IndexModify<K, V>,
    T: Index<K, V>,
{
    fn apply(&mut self, adds: &[KeyChanges<K, V>]) -> PRes<()> {
        let mut updates = Vec::new();
        let mut prev_leaf_id = None;
        for add in adds {
            let mut next: Option<PosRef<K>> = None;
            let mut path = Vec::new();
            loop {
                next = if let Some(cur_node) = &mut next {
                    let mut new_next = None;
                    if let Some((node_modify, version)) = self.load_modify(&cur_node.node_ref)? {
                        if let Node::NODE(n) = &*node_modify {
                            cur_node.version = version;
                            path.push(cur_node.clone());
                            new_next = n.find_write(&add.k);
                        } else if let Node::LEAF(ref _none) = &*node_modify {
                            cur_node.version = version;
                            if let Some((leaf_modify, version)) = lock_logic(self, cur_node, node_modify, version)? {
                                path.push(cur_node.clone());
                                if let Node::LEAF(mut leaf) = self.owned(&cur_node.node_ref, leaf_modify) {
                                    let node_ref = &cur_node.node_ref;
                                    if add.apply(&mut leaf, self.value_mode(), self.index_name())? {
                                        self.update(node_ref, Node::LEAF(leaf), version)?;
                                        if Some(node_ref.clone()) != prev_leaf_id {
                                            updates.push(path);
                                        }
                                        prev_leaf_id = Some(node_ref.clone())
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    new_next
                } else if let Some(r) = self.get_root_refresh()? {
                    path.clear();
                    Some(PosRef::new(&add.k, 0, r.clone(), None))
                } else {
                    self.lock_config()?;
                    if let Some(r) = self.get_root_refresh()? {
                        path.clear();
                        Some(PosRef::new(&add.k, 0, r.clone(), None))
                    } else {
                        let mut leaf = Leaf::new();
                        add.apply(&mut leaf, self.value_mode(), self.index_name())?;
                        let leaf_ref = self.insert(Node::LEAF(leaf))?;
                        self.set_root(Some(leaf_ref))?;
                        break;
                    }
                }
            }
        }
        while updates.len() > 1 || (updates.len() == 1 && updates[0].len() > 1) {
            updates = lock_parents(self, updates)?;
            let parent_updates = group_by_parent(updates);
            updates = Vec::new();
            for update in parent_updates {
                let parent_id = update.path.last().unwrap().node_ref.clone();
                // It's locked, should not have miss read unwrap.
                let (read_node, n_version) = self.load_modify(&parent_id)?.unwrap();
                if let Node::NODE(mut n) = self.owned(&parent_id, read_node) {
                    let mut save = false;
                    let mut flags = vec![false; n.len()];
                    for ch in update.children {
                        let pos = n.find(&ch.k);
                        flags[pos.pos] = true;
                    }
                    let mut i = 0;
                    while i < n.len() {
                        let pos = i;
                        i += 1;
                        if flags[pos] && n.len() > 1 {
                            // It's locked, should not have miss read unwrap.
                            let cur_id = n.get(pos);
                            let (cur, version) = self.load_modify(&cur_id)?.unwrap();
                            if cur.len() < self.bottom_limit() {
                                let mut new_pos = pos;
                                let to_modify = self.owned(&cur_id, cur);
                                if merge_and_save(self, &mut n, pos, to_modify, version)? {
                                    new_pos += 1;
                                }
                                flags.remove(new_pos);
                                flags[new_pos - 1] = true;
                                i = new_pos - 1;
                                save = true;
                            }
                        }
                    }
                    for pos in 0..n.len() {
                        if flags[pos] {
                            // It's locked, should not have miss read unwrap.
                            let cur_id = n.get(pos);
                            let (cur, cur_version) = self.load_modify(&cur_id)?.unwrap();
                            if cur.len() > self.top_limit() {
                                let to_modify = self.owned(&cur_id, cur);
                                let mut ids = split_link_save(self, n.get(pos), to_modify, cur_version)?;
                                for _ in 0..ids.len() {
                                    flags.insert(pos, false);
                                }
                                n.insert_after(pos, &mut ids);
                                save = true;
                            }
                        }
                    }
                    if save {
                        self.update(&parent_id, Node::NODE(n), n_version)?;
                        if !update.path.is_empty() {
                            updates.push(update.path);
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if updates.len() == 1 && updates[0].len() == 1 {
            self.lock_config()?;
            while let Some(r) = self.get_root_refresh()? {
                if let Some((n, n_version)) = self.load_modify(&r)? {
                    if n.len() > self.top_limit() {
                        let to_modify = self.owned(&r, n);
                        let ids = split_link_save(self, r.clone(), to_modify, n_version)?;
                        let node = Node::NODE(Nodes::new_from_split(r, &ids));
                        let cur_id = self.insert(node)?;
                        self.set_root(Some(cur_id))?;
                    } else if n.len() == 1 {
                        if let Node::NODE(cn) = &*n {
                            self.delete(&r, n_version)?;
                            self.set_root(Some(cn.get(0)))?;
                        } else {
                            break;
                        }
                    } else if n.len() == 0 {
                        self.set_root(None)?;
                    } else {
                        break;
                    }
                }
            }
        }
        Ok(())
    }
}

impl<K: IndexType, V: IndexType, T> Index<K, V> for T
where
    T: IndexKeeper<K, V>,
{
    fn get(&mut self, k: &K) -> PRes<Option<Value<V>>> {
        Ok(if let Some(node) = self.get_root()? {
            let mut cur_node = node;
            loop {
                match self.load(&cur_node)? {
                    Node::NODE(n) => {
                        cur_node = n.find(k).node_ref;
                    }
                    Node::LEAF(leaf) => {
                        break leaf.find(k).map(|el| el.1).ok();
                    }
                }
            }
        } else {
            None
        })
    }

    fn iter_from(&mut self, first: Bound<&K>) -> PRes<PageIter<K, V>> {
        let mut path = Vec::new();
        let mut iter = if let Some(mut cur_node) = self.get_root()? {
            path.push((0, cur_node.clone()));
            loop {
                match self.load(&cur_node)? {
                    Node::NODE(n) => {
                        let value = match first {
                            Bound::Included(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Excluded(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Unbounded => (0, n.get(0)),
                        };
                        cur_node = value.1.clone();
                        path.push(value);
                    }
                    Node::LEAF(leaf) => {
                        break PageIter {
                            iter: leaf.iter_from(first).peekable(),
                        };
                    }
                }
            }
        } else {
            PageIter {
                iter: Vec::new().into_iter().peekable(),
            }
        };

        while iter.iter.peek().is_none() {
            let prev = path.pop();
            if let Some((_, node)) = path.last() {
                let (pos, _) = prev.unwrap();
                match self.load(&node)? {
                    Node::NODE(n) => {
                        // check if there are more elements in the node
                        if n.len() > pos + 1 {
                            let mut cur_node = n.get(pos + 1);
                            loop {
                                match self.load(&cur_node)? {
                                    Node::NODE(n) => {
                                        cur_node = n.get(0);
                                    }
                                    Node::LEAF(leaf) => {
                                        // Use here the key anyway, should start from the first in
                                        // any case
                                        iter = PageIter {
                                            iter: leaf.iter_from(first).peekable(),
                                        };
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    Node::LEAF(_leaf) => {
                        panic!("can't happen");
                    }
                }
            } else {
                break;
            }
        }
        Ok(iter)
    }

    fn back_iter_from(&mut self, last: Bound<&K>) -> PRes<PageIterBack<K, V>> {
        let mut path = Vec::new();
        let mut iter = if let Some(mut cur_node) = self.get_root()? {
            path.push((0, cur_node.clone()));
            loop {
                match self.load(&cur_node)? {
                    Node::NODE(n) => {
                        let value = match last {
                            Bound::Included(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Excluded(f) => {
                                let found = n.find(f);
                                (found.pos, found.node_ref)
                            }
                            Bound::Unbounded => {
                                let pos = n.len() - 1;
                                (pos, n.get(pos))
                            }
                        };
                        cur_node = value.1.clone();
                        path.push(value);
                    }
                    Node::LEAF(leaf) => {
                        break PageIterBack {
                            iter: leaf.back_iter_from(last).peekable(),
                        };
                    }
                }
            }
        } else {
            PageIterBack {
                iter: Vec::new().into_iter().rev().peekable(),
            }
        };

        while iter.iter.peek().is_none() {
            let prev = path.pop();
            if let Some((_, node)) = path.last() {
                let (pos, _) = prev.unwrap();
                match self.load(&node)? {
                    Node::NODE(n) => {
                        // check if there are more elements in the node
                        if pos > 0 {
                            let mut cur_node = n.get(pos - 1);
                            loop {
                                match self.load(&cur_node)? {
                                    Node::NODE(n) => {
                                        cur_node = n.get(n.len() - 1);
                                    }
                                    Node::LEAF(leaf) => {
                                        // Use here the key anyway, should start from the first in
                                        // any case
                                        iter = PageIterBack {
                                            iter: leaf.back_iter_from(last).peekable(),
                                        };
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    Node::LEAF(_leaf) => {
                        panic!("can't happen");
                    }
                }
            } else {
                break;
            }
        }
        Ok(iter)
    }
}

#[cfg(test)]
mod tests {
    use super::{
        Index, IndexApply, IndexKeeper, IndexModify, IndexType, KeyChanges, Leaf, Node, NodeRef, Nodes, PRes, Value,
        ValueChange, ValueMode,
    };
    use crate::{error::PersyError, id::RecRef};
    use rand::random;
    use std::{collections::HashMap, ops::Bound, rc::Rc};

    impl<V> std::fmt::Display for Value<V>
    where
        V: std::fmt::Display,
    {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            match self {
                Value::CLUSTER(x) => {
                    write!(f, "{} values: [", x.len())?;
                    for v in x {
                        write!(f, " {}, ", v)?;
                    }
                    write!(f, "]")?;
                }
                Value::SINGLE(v) => {
                    write!(f, "value: {}", v)?;
                }
            }
            Ok(())
        }
    }

    fn print_nodes<K: IndexType, V: IndexType>(
        tree: &mut dyn IndexKeeper<K, V>,
        node: &Nodes<K>,
        depth: usize,
    ) -> PRes<()> {
        let padding = String::from_utf8(vec![b' '; depth]).unwrap();
        for i in 0..node.len() {
            if i == 0 {
                println!("{} __ ", padding);
            } else {
                println!("{} {}  ", padding, node.keys[i - 1]);
            }
            print_node(tree, &node.pointers[i], depth + 1)?;
        }
        Ok(())
    }

    fn print_leaf<K: IndexType, V: IndexType>(
        _tree: &mut dyn IndexKeeper<K, V>,
        leaf: &Leaf<K, V>,
        depth: usize,
    ) -> PRes<()> {
        let padding = String::from_utf8(vec![b' '; depth]).unwrap();
        println!("{} ", padding);
        for i in 0..leaf.len() {
            println!("{} {} {} ", padding, leaf.entries[i].key, leaf.entries[i].value);
        }
        println!("{} ", padding);
        Ok(())
    }

    fn print_node<K: IndexType, V: IndexType>(
        tree: &mut dyn IndexKeeper<K, V>,
        node: &NodeRef,
        depth: usize,
    ) -> PRes<()> {
        match tree.load(node)? {
            Node::NODE(n) => {
                print_nodes(tree, &n, depth)?;
            }
            Node::LEAF(l) => {
                print_leaf(tree, &l, depth)?;
            }
        }
        Ok(())
    }

    fn print_tree<K: IndexType, V: IndexType>(tree: &mut dyn IndexKeeper<K, V>) -> PRes<()> {
        let root = tree.get_root()?;
        if let Some(r) = root {
            print_node(tree, &r, 0)?;
        } else {
            println!(" Empty Root");
        }

        Ok(())
    }
    fn random_pointer() -> NodeRef {
        RecRef::new(random::<u64>(), random::<u32>())
    }

    struct MockIndexKeeper<K: Clone + Ord, V: Clone> {
        store: HashMap<NodeRef, Node<K, V>>,
        root: Option<NodeRef>,
        v: ValueMode,
        name: String,
    }

    impl<K: Clone + Ord, V: Clone> MockIndexKeeper<K, V> {
        fn new() -> MockIndexKeeper<K, V> {
            MockIndexKeeper {
                store: HashMap::new(),
                root: None,
                v: ValueMode::REPLACE,
                name: "test_index".to_string(),
            }
        }

        fn new_mode(v: ValueMode) -> MockIndexKeeper<K, V> {
            MockIndexKeeper {
                store: HashMap::new(),
                root: None,
                v,
                name: "test_index".to_string(),
            }
        }
    }

    impl<K: Clone + Ord, V: Clone> IndexKeeper<K, V> for MockIndexKeeper<K, V> {
        fn load(&self, node: &NodeRef) -> PRes<Node<K, V>> {
            Ok(self.store.get(&node).unwrap().clone())
        }
        fn get_root(&self) -> PRes<Option<NodeRef>> {
            Ok(self.root.clone())
        }

        fn value_mode(&self) -> ValueMode {
            self.v.clone()
        }

        fn index_name(&self) -> &String {
            &self.name
        }
    }

    impl<K: Clone + Ord, V: Clone> IndexModify<K, V> for MockIndexKeeper<K, V> {
        fn load_modify(&self, node: &NodeRef) -> PRes<Option<(Rc<Node<K, V>>, u16)>> {
            Ok(Some((Rc::new(self.store.get(&node).unwrap().clone()), 0)))
        }

        fn lock(&mut self, _node: &NodeRef, _version: u16) -> PRes<bool> {
            Ok(true)
        }

        fn unlock(&mut self, _node: &NodeRef) -> PRes<bool> {
            Ok(true)
        }
        fn unlock_config(&mut self) -> PRes<bool> {
            Ok(true)
        }

        fn lock_config(&mut self) -> PRes<bool> {
            Ok(true)
        }

        fn owned(&mut self, _node_ref: &NodeRef, mut node: Rc<Node<K, V>>) -> Node<K, V> {
            Rc::make_mut(&mut node);
            Rc::try_unwrap(node).ok().unwrap()
        }
        fn insert(&mut self, node: Node<K, V>) -> PRes<NodeRef> {
            let node_ref = random_pointer();
            self.store.insert(node_ref.clone(), node.clone());
            Ok(node_ref)
        }

        fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>, _version: u16) -> PRes<()> {
            self.store.insert(node_ref.clone(), node);
            Ok(())
        }

        fn get_root_refresh(&mut self) -> PRes<Option<NodeRef>> {
            Ok(self.root.clone())
        }

        fn set_root(&mut self, root: Option<NodeRef>) -> PRes<()> {
            Ok(self.root = root)
        }

        fn delete(&mut self, node: &NodeRef, _version: u16) -> PRes<()> {
            self.store.remove(&node);
            Ok(())
        }
        fn bottom_limit(&self) -> usize {
            10
        }
        fn top_limit(&self) -> usize {
            30
        }
    }

    #[test]
    fn test_single_add() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        changes.push(KeyChanges::single_add(1, 1));
        changes.push(KeyChanges::single_add(2, 2));
        changes.push(KeyChanges::single_add(3, 4));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(2))));
    }

    #[test]
    fn test_many_add() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for i in 0..200 {
            changes.push(KeyChanges::single_add(i, i));
        }
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(2))));
        assert_eq!(keeper.get(&100).ok(), Some(Some(Value::SINGLE(100))));
        assert_eq!(keeper.get(&201).ok(), Some(None));
    }

    #[test]
    fn test_many_add_multiple_times() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for n in 0..8 {
            for i in 0..20 {
                changes.push(KeyChanges::single_add(i, i));
            }
            keeper.apply(&changes).unwrap();
            assert_eq!(keeper.get(&(n + 2)).ok(), Some(Some(Value::SINGLE(n + 2))));
        }
    }

    #[test]
    fn test_single_add_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        changes.push(KeyChanges::single_add(1, 1));
        changes.push(KeyChanges::single_add(2, 2));
        changes.push(KeyChanges::single_add(3, 4));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(2))));

        let mut changes = Vec::new();
        changes.push(KeyChanges::single_delete(1, Some(1)));
        changes.push(KeyChanges::single_delete(2, Some(2)));
        changes.push(KeyChanges::single_delete(3, Some(4)));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(None));
    }

    #[test]
    fn test_aggregate_add_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![ValueChange::ADD(1), ValueChange::REMOVE(Some(1)), ValueChange::ADD(2)],
        ));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(2))));

        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![
                ValueChange::REMOVE(Some(2)),
                ValueChange::ADD(1),
                ValueChange::REMOVE(Some(1)),
            ],
        ));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(None));
    }

    #[test]
    fn test_group_replace_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::ADD(1), ValueChange::ADD(2)]));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(2))));

        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::REMOVE(Some(2))]));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(None));
    }

    #[test]
    fn test_group_values_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new_mode(ValueMode::CLUSTER);
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![ValueChange::ADD(1), ValueChange::ADD(2), ValueChange::ADD(3)],
        ));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::CLUSTER(vec![1, 2, 3]))));

        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![ValueChange::REMOVE(Some(1)), ValueChange::REMOVE(Some(2))],
        ));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(3))));
    }

    #[test]
    fn test_group_values_remove_no_order() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new_mode(ValueMode::CLUSTER);
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![ValueChange::ADD(3), ValueChange::ADD(1), ValueChange::ADD(2)],
        ));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::CLUSTER(vec![3, 1, 2]))));

        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![ValueChange::REMOVE(Some(1)), ValueChange::REMOVE(Some(2))],
        ));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(3))));
    }

    #[test]
    fn test_add_same_value_twice() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new_mode(ValueMode::CLUSTER);
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(
            2,
            vec![ValueChange::ADD(1), ValueChange::ADD(2), ValueChange::ADD(1)],
        ));

        changes.push(KeyChanges::new(1, vec![ValueChange::ADD(1), ValueChange::ADD(1)]));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::CLUSTER(vec![1, 2]))));
        assert_eq!(keeper.get(&1).ok(), Some(Some(Value::SINGLE(1))));
    }

    #[test]
    fn test_add_to_esclusive() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new_mode(ValueMode::EXCLUSIVE);
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::ADD(1)]));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(1))));
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::ADD(1)]));

        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(1))));
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::ADD(2)]));
        assert!(match keeper.apply(&changes) {
            Err(PersyError::IndexDuplicateKey(ref idxname, ref keyname))
                if (idxname == "test_index" && keyname == "2") =>
            {
                true
            }
            _ => false,
        });
    }

    #[test]
    fn test_group_key_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new_mode(ValueMode::CLUSTER);
        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::ADD(1), ValueChange::ADD(2)]));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::CLUSTER(vec![1, 2]))));

        let mut changes = Vec::new();
        changes.push(KeyChanges::new(2, vec![ValueChange::REMOVE(None)]));
        keeper.apply(&changes).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(None));
    }

    #[test]
    fn test_many_add_remove() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for i in 0..200 {
            changes.push(KeyChanges::single_add(i, i));
        }
        changes.sort_by_key(|k| k.k);
        keeper.apply(&changes).unwrap();
        print_tree(&mut keeper).unwrap();
        assert_eq!(keeper.get(&2).ok(), Some(Some(Value::SINGLE(2))));
        assert_eq!(keeper.get(&100).ok(), Some(Some(Value::SINGLE(100))));
        assert_eq!(keeper.get(&201).ok(), Some(None));
        let mut changes = Vec::new();
        for i in 0..200 {
            changes.push(KeyChanges::single_delete(i, Some(i)));
        }
        changes.sort_by_key(|k| k.k);
        keeper.apply(&changes).unwrap();
        print_tree(&mut keeper).unwrap();
        assert_eq!(keeper.get_root().ok(), Some(None));
        assert_eq!(keeper.get(&2).ok(), Some(None));
        assert_eq!(keeper.get(&100).ok(), Some(None));
    }

    #[test]
    fn test_many_add_remove_multiple_times() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        let mut rchanges = Vec::new();
        for n in 0..8 {
            for i in 0..20 {
                changes.push(KeyChanges::single_add(i, i));
                rchanges.push(KeyChanges::single_delete(i, Some(i)));
            }
            changes.sort_by_key(|k| k.k);
            keeper.apply(&changes).unwrap();
            assert_eq!(keeper.get(&(n + 2)).ok(), Some(Some(Value::SINGLE(n + 2))));
            rchanges.sort_by_key(|k| k.k);
            keeper.apply(&rchanges).unwrap();
            assert_eq!(keeper.get(&(n + 2)).ok(), Some(None));
        }
    }

    #[test]
    fn test_iter_from() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for i in 0..50 {
            changes.push(KeyChanges::single_add(i, i));
        }
        keeper.apply(&changes).unwrap();
        print_tree(&mut keeper).unwrap();
        let mut iter = keeper.iter_from(Bound::Included(&5)).unwrap();
        let next = iter.iter.next();

        assert_eq!(5, next.unwrap().key);
        let next = iter.iter.next();
        assert_eq!(6, next.unwrap().key);
        let mut last_val = None;
        for v in iter.iter {
            last_val = Some(v);
        }
        let mut next_page = keeper
            .iter_from(Bound::Excluded(&last_val.clone().unwrap().key))
            .unwrap();
        let next = next_page.iter.next();
        assert_eq!(last_val.unwrap().key + 1, next.unwrap().key);
    }

    #[test]
    fn test_iter() {
        let mut keeper = MockIndexKeeper::<u8, u8>::new();
        let mut changes = Vec::new();
        for i in 0..50 {
            changes.push(KeyChanges::single_add(i, i));
        }
        keeper.apply(&changes).unwrap();
        print_tree(&mut keeper).unwrap();
        let mut iter = keeper.iter_from(Bound::Unbounded).unwrap();
        let next = iter.iter.next();

        assert_eq!(0, next.unwrap().key);
        let mut last_val = None;
        for v in iter.iter {
            last_val = Some(v);
        }
        let mut next_page = keeper
            .iter_from(Bound::Excluded(&last_val.clone().unwrap().key))
            .unwrap();
        let next = next_page.iter.next();
        assert_eq!(last_val.unwrap().key + 1, next.unwrap().key);
        let mut last_val = None;
        for v in next_page.iter {
            last_val = Some(v);
        }
        assert_eq!(last_val.unwrap().key, 49);
        let mut next_page = keeper.iter_from(Bound::Excluded(&49)).unwrap();
        assert!(next_page.iter.next().is_none());
    }

    #[test]
    fn test_a_lot_add_remove_multiple_times() {
        let mut keeper = MockIndexKeeper::<u32, u32>::new();
        let mut changes = Vec::new();
        let mut remove = Vec::new();
        for n in 1..30 {
            for i in 1..200 {
                changes.push(KeyChanges::single_add(i * n, i * n));
                if i % 2 == 0 {
                    remove.push(KeyChanges::single_delete(i * n, Some(i * n)));
                }
            }
            changes.sort_by_key(|k| k.k);
            keeper.apply(&changes).unwrap();
            assert_eq!(keeper.get(&(2 * n)).ok(), Some(Some(Value::SINGLE(2 * n))));
            assert_eq!(keeper.get(&(100 * n)).ok(), Some(Some(Value::SINGLE(100 * n))));
            assert_eq!(keeper.get(&20001).ok(), Some(None));
            remove.sort_by_key(|k| k.k);
            keeper.apply(&remove).unwrap();
            assert_eq!(keeper.get(&(2 * n)).ok(), Some(None));
            assert_eq!(keeper.get(&(100 * n)).ok(), Some(None));
        }
    }

    #[test]
    fn test_big_tree() {
        let mut keeper = MockIndexKeeper::<u32, u32>::new();
        for n in 1..20 {
            let mut changes = Vec::new();
            let mut remove = Vec::new();
            for i in 1..301 {
                changes.push(KeyChanges::single_add(i * n, i * n));
                if i % 2 == 0 {
                    remove.push(KeyChanges::single_delete(i * n, Some(i * n)));
                }
            }
            keeper.apply(&changes).unwrap();
            assert_eq!(keeper.get(&(2 * n)).ok(), Some(Some(Value::SINGLE(2 * n))));
            assert_eq!(keeper.get(&(100 * n)).ok(), Some(Some(Value::SINGLE(100 * n))));
            assert_eq!(keeper.get(&20001).ok(), Some(None));

            keeper.apply(&remove).unwrap();
            assert_eq!(keeper.get(&(2 * n)).ok(), Some(None));
            assert_eq!(keeper.get(&(100 * n)).ok(), Some(None));
        }
    }
}
