mod helpers;
use helpers::create_and_drop;
use persy::Persy;
use std::thread;

#[test]
fn test_multithread_many_key_insert() {
    create_and_drop("multithread_many_key_insert", |persy| {
        let mut tx = persy.begin().expect("persy begin");
        tx.create_index::<i32, i32>("tdbi", persy::ValueMode::REPLACE)
            .expect("persy create_index");
        let prepared = tx.prepare().expect("persy prepare_commit");
        prepared.commit().expect("persy commit");

        let my_insert = |persy: &Persy, key: i32, value: i32| {
            let mut tx = persy.begin().expect("persy begin");
            tx.put::<i32, i32>("tdbi", key, value).expect("persy put");
            let prepared = tx.prepare().expect("persy prepare_commit");
            prepared.commit().expect("persy commit");
        };

        let spawn_worker = |r| {
            let own_persy = persy.clone();
            thread::spawn(move || {
                for i in r {
                    my_insert(&own_persy, i, i);
                }
            })
        };

        let t_1 = spawn_worker(0..1000);
        let t_2 = spawn_worker(0..1000);
        let t_3 = spawn_worker(1000..2000);
        let t_4 = spawn_worker(1000..2000);

        assert!(t_1.join().is_ok());
        assert!(t_2.join().is_ok());
        assert!(t_3.join().is_ok());
        assert!(t_4.join().is_ok());
        for x in 0..2000 {
            assert!(persy.get::<i32, i32>("tdbi", &x).unwrap().is_some());
        }
        let range_iter = persy.range::<i32, i32, _>("tdbi", ..).unwrap();
        assert_eq!(range_iter.count(), 2000);
        let range_iter = persy.range::<i32, i32, _>("tdbi", ..).unwrap();
        assert_eq!(range_iter.rev().count(), 2000);
    });
}
