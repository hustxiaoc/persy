# Persy
[![build status](https://gitlab.com/tglman/persy/badges/master/build.svg)](https://gitlab.com/tglman/persy/commits/master)
[![coverage report](https://gitlab.com/tglman/persy/badges/master/coverage.svg)](https://gitlab.com/tglman/persy/commits/master)

[Persy](https://persy.rs) is a transactional storage engine written in rust.

## COMPILING THE SOURCE

Checkout the source code:

```
git clone https://gitlab.com/tglman/persy.git
```

Compile and Test

```
cargo test
```

## INSTALL

Add it as dependency of your project:

```toml
[dependencies]
persy="0.11"
```

## USAGE EXAMPLE

Create a new persy file save some data in it and scan it.

```rust
use persy::{Persy,Config};
//...
Persy::create("./open.persy")?;
let persy = Persy::open("./open.persy",Config::new())?;
let mut tx = persy.begin()?;
tx.create_segment("seg")?;
let data = vec![1;20];
tx.insert("seg", &data)?;
let prepared = tx.prepare()?;
prepared.commit()?;
for (_id, content) in persy.scan("seg")? {
    assert_eq!(content[0], 20);
    //....
}
```

## LINKS

[crates.io](https://crates.io/crates/persy)  
[docs.rs](https://docs.rs/persy/)  
[Mastodon](https://mastodon.technology/@persy_rs)  
[persy.rs](https://persy.rs)  
[Issues](https://gitlab.com/tglman/persy/-/issues)
[Repository](https://gitlab.com/tglman/persy)

## CONTRIBUTION

There are a few option for contribute to persy, you can start from reviewing and suggesting API changes, jump directly to hacking the code or just playing a bit with docs.
If you want a list of possibility you can start from the list of [new contributor friendly issues](https://gitlab.com/tglman/persy/-/issues?label_name%5B%5D=new+contributor+friendly)
If you would like to contact the developers send a mail to user `dev` on the `persy.rs` domain.
